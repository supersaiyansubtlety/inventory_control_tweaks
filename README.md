<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Inventory Control Tweaks

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_421375_all.svg)](https://modrinth.com/mod/inventory-control-tweaks/versions#all-versions)
![environment: client*](https://img.shields.io/badge/environment-client*-1976d2)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Cloth Config](https://img.shields.io/badge/supports-Cloth_Config-89dd43?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAyNjQgMjY0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiPjxwYXRoIGlkPSJiYWNrZ3JvdW5kIiBkPSJNODQsMjA3bC0zNiwtMzZsMCwtMzlsMTgsLTE4bDE4LC0yMWwyMSwtMThsMzYsLTM2bDAsLTIxbDIxLDBsMTgsMjFsMzYsMzZsMCwxOGwtNTQsNzhsLTM5LDM2bC0zOSwwWiIgc3R5bGU9ImZpbGw6IzljZmY1NTsiLz48cGF0aCBpZD0ib3V0bGluZSIgZD0iTTE2MiwxOGwwLC0xOGwtMjEsMGwwLDE4bC0xOCwwbDAsMzlsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMTgsMGwwLDE4bC0yMSwwbDAsMzlsMjEsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDE4bDIxLDBsMCwtMThsMTgsMGwwLC0yMWwxOCwwbDAsLTE4bDIxLDBsMCwtMThsMTgsMGwwLC0xOGwxOCwwbDAsLTIxbDM5LDBsMCwtMThsMTgsMGwwLC0zOWwtMTgsMGwwLC0xOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLDIxbDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDM5bDIxLDBsMCwxOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMjEsMGwwLDE4bDIxLDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDM2bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMThsLTE4LDBsMCwyMWwtMjEsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0yMWwyMSwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0zOWwyMSwwIiBzdHlsZT0iZmlsbDojMTE1MTEwOyIvPjxwYXRoIGlkPSJzaGFkZS0xIiBzZXJpZjppZD0ic2hhZGUgMSIgZD0iTTQ4LDE3MWwtMTgsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDIxbDIxLDBsMCwtMjFsLTIxLDBsMCwtMThsLTE4LDBsMCwtMThsLTE4LDBsMCwtMThabTE2OCwtNTdsMjEsMGwwLDE4bC0yMSwwbDAsLTE4WiIgc3R5bGU9ImZpbGw6IzZlYWEzMzsiLz48cGF0aCBpZD0ic2hhZGUtMiIgc2VyaWY6aWQ9InNoYWRlIDIiIGQ9Ik00OCwxNTBsLTE4LDBsMCwyMWwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwxOGwyMSwwbDAsLTE4bC0yMSwwbDAsLTE4bC0xOCwwbDAsLTE4bC0xOCwwbDAsLTIxWiIgc3R5bGU9ImZpbGw6IzgwY2MzZDsiLz48cGF0aCBpZD0ic2hhZGUtMyIgc2VyaWY6aWQ9InNoYWRlIDMiIGQ9Ik0xMDUsMjI1bDAsLTE4bDE4LDBsMCwtMzZsMzksMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwzNmwtMTgsMGwwLDIxbC0xOCwwbDAsMThsLTIxLDBsMCwxOGwtMTgsMGwwLDE4bC0xOCwwWm0xMTEsLTExMWwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwyMVoiIHN0eWxlPSJmaWxsOiM4OWRjNDI7Ii8+PHBhdGggaWQ9InNoYWRlLTQiIHNlcmlmOmlkPSJzaGFkZSA0IiBkPSJNMTQxLDE3MWwtMzYsMGwwLC0yMWwtMjEsMGwwLC0xOGwyMSwwbDAsMThsMzYsMGwwLDIxWm0zOSwtMzlsLTM5LDBsMCwtMThsLTE4LDBsMCwtMjFsLTE4LDBsMCwtMThsMTgsMGwwLDE4bDE4LDBsMCwyMWwzOSwwbDAsMThabS0xOCwtNzVsLTIxLDBsMCwtMThsMjEsMGwwLDE4WiIgc3R5bGU9ImZpbGw6IzkwZWI0YjsiLz48L3N2Zz4=)](https://modrinth.com/mod/cloth-config/versions)
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)
[![supports: Pick Block Pro](https://img.shields.io/badge/supports-Pick_Block_Pro-8dda68?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH6AYFBRY6UgKYUwAAAAZiS0dEAP8A/wD/oL2nkwAAD/pJREFUeNq1WAl0VdW5/vYZ7rlzcm8SyAiZQxKSQAYIYFEBUZzAKGqRotVaarU4oYBFsbY8rc8HWn3Vp9Q+h+prHYvVhwqiMmpRQBA0IQNDyHgz545neF/uCpb1VlefC1+/tf679zlnn39/9/v/vffZG2eC6upq0AQNf7LbxdMJCdKnALYBGz8A3rEAWGlpYq2qitPbngHOjNRoKVdVVanzi4uVLgD/AWjbgbZNQHA64L8awNyCApVtRkymCRpOld8W0rclhr9BoQmaEYlE9HsOHzaOjt4gTJplACIGYHJDg6ETQghDEAAUy7JAxIlOnjz5O6smRg2jisllZWWYMGGCqKysLOX1bTXV1S9X1dQcqSkuvnoP0LAF6B6pV0+ZcpjPXmKbn7HthPXr12PixInf+GGJmpoawWfiu5CLl3SolJSUgM5k1q/gvW10brK0eE8vq6iwJubk3LwXOLQVaK8qLFxSMXnyyLN4G5pB+5DvLpg0aZI0e/bsuE/WBQ1xkmeSa6P5otABaNXEbhKzWLeoRl9BQcEfsjIzfmDzeEqq0x2e7RJOvCUQTPPbxrj8SbnZ48dfRbU3lJeXd9KPNUKW5Ta+X0ED6wrDLGigfSuSf5ccy9t4L8rSKi0tPZmVlXWXLMtjQGT4/eI3iiQ9qUh4RhLXrZHE3V9pMiyHImUmJwtFUWC32xPy8vKWkmjjKNEwy5tGxfhGSZo4E3KPjKjGl62cnJxnACSBqCyrEjf86yXapSunqXe/ewEe2FGX9uMDFz/5k4MXv3D/7styV350PhbcO1259sELtcuvvUjYnTaQrKu4uPgh+o2Npscv6Rs0hRERaWlpYCn+r9AKkpJPJ8eBMZSamrqI15h11lzp8jtmazc8dYG6YEMV6l6qsq/45NK77/u8ru+unfOsu3bMs+7fe0Xw53+9bO31b8/0Lnn9e7j5xUuUq1eer31vxtlxv/n5+eczpJ2nk2RdodKCdZGZmfkP1VNooN1+ilxKSsp547KzcHZdjbbosbNslrVTHLdewZrPLp+/fNeF9Xfummc98MXVxvLdF+m37zhf//meBcaybedZt22be/zePXWLUQ7hL5Fx/e9mqectnq4VT5wAkpxEkh2jJH96et9MBYFRSKfUG52fZJpOqxVCPByLxRAIBG602xzvT5yX5ZiyLD0287Lq6JMtr5S90vD++6rN9qZpWgWxmGW4HF7h9/hkt90p2xSblOpPNVJ8KRk2m/rCuldv3PEvby+rrZlVFjtrZUas8LyxDqHL+8Lh8CL2C/b1KPuqNgxDN01TpiCg/Y0gGwkCJtHZ2SmIh3lPGRwcfHx8at7LVXX5WtUNWSHNqSZ19pz8bX+w6zPd1OcMBIeM/uEhMxyNyD39nSIWDUMSEixY0FRNjuo6BoLDBttO6xnu2tnee+I5M4y0CfN9oeQKp5boTNyix/TV7E/loHt44cKFKCoqMjs6OpDMAYbTw3taaK8eCS2TtRlA8riiVPxh7yPail0Lli37+LzAnbsutFbvucxY83mdfhfrvzp4jfVo41KG+Crrtu1zLYbYemD/Vda6hhutBw4sil+vZsh/sW+hsWrPfOuWj2YPLN956crX2x9xvNn3EMYXZNgZ4r3s16qoqFjIOhh65dRgkU5bFw1JkiCEWEqpEQoH182+alr3pffV5nU4Gj7LSE17zOVw+gK9A8bJQEAKx2KyQ7NDAsDwABbg5LXf64c/ISWeMsOhIej0RaeS1+2X/O5kI8HtdeWOy3zwYMvB/ct/9NuCxExf2LTMh9kv2P9N2dnZKCwsNOgTqqpCTk9PF6OhNolyNvw1H3a09R27pfyqzGHPWK1mMDx8R6CnN5bgcUg2TZF0IxYPo8vpgceRAHbAUPZDFhJiuoH2ni7EjAhcdic8bEOy6BvqAxRdssk27N5ar7/27LaUzkDgL12RrkYriGa34lnIvqva2tpe7+7u7uCUJFMoSPwBIUCwwbkgotHonyd6azv7srpwqLHFOH6iHXv31cu79n4lguEIvG43/J4EuDQXIrEw+gYDCIZD0E0DzEf0Dw3GidpUDZrigM0mQ7YJfPnX43j2sa3izdd2ycxfOJI0w8gzkKcWDvNPvAqCqs3RNA0sRVdXF6Tl9fUYFwwiqiiQhJhqmSaikcimtkAb9IYoIi0h0draCeXjICIHhtHa3gszokBYCugEgAWOcuRn5MPrTEBygh9VReVIdHkRV9llR29nGO//8Uu8+fs96NjXDX8zibttoPJCNgErGo/f5tEwT1O8XpRS9SdGOJnMjzfmzTOSydoAymImwzU4uLfJYog2hOHYGIJrswF/wAu5Pobu3gF0HOtB87F21Dcep9oxOrXQ3R9AY+txNBw/hq7+bqh2gUgwhk0bP8d/btiCL/Y0w5FmgyukQmozEIvQggxhn8B27RnolnXQBHTapN0ffIAX9u7VQ+SmdEWj1u/XrbNdBxhlublPcmrwTOnubl+yrEb66NAx05Jk9HT2IXvqDATQhgP1e7HHGELqOC8Up8CBfTqKK7LgcisYk+SHLEsYZD5+sbsHn+48gh7+IZuhwCYUYBCQ+y3IDgHRTeUDgCUpsFAorztq67k1M/NOXdfV+4SwvydJ5secF5XaWOx1CZi+Gwi/2NQ0xLoZBJYHnti0bHpR+suNhelycpYPwd6jkKMyEj42II2T0ScPImGcC7Iq4eiJTni9LhRlJEEPWnjnpQM4crANDq8N8pDClDFhS5KgfWpAHQZkpwTXIQuOiCVfYhhYYGKBA9GnX2pqagcRBG4VpqnNBT5VuoAUIG6WSpICMEKA+ysL2Xp/EC1tvcLlsiHW0wzVpiJnrA9t9YPQS10wXCrczDF98yACSSFsSmhDSz0HzNch+DpVhJ1U66QJzS7DzJShGDpUTQLzCrAsmBpEJAoEonDaAL8CaACkGOCQAHQAZfJW4OUtwIZNwPrt+fkN7/t8773S2/vTzsL0zb7CdMPldmRHorElHQMRSxDjx3iQk5qAoSRApxKx1/sgNcVgsxTYp3hw7FAAaoMFrXskpFTpJKAKAbSasIcBi9VgzIj74s9zX8ui6U8wD31oiqd25Ocf2+zzvf3n3t6fkc/jG4F10qNeb6Tthz88mjVz5jGbz7dM9fme1HJyorvru6O1E1KRl+FH8Xg/R6iK9v5h7G/phuZS4PkshuhrvdDYcWJMgcMlQ0gCoteEbHmgeseQpMXQU+Uw1eux4LKrkISIm00WUEcMBoY27tFTqqpiNr//N5LXu/IocLwFaJ7vcg1JLweD1vQPP1Q4EYFTzJfCsiS33V6eWlCErxvbcbi5EwPDUaR4HEh0aIjEDAT6QyAXuHoFfF6NK4cdwjDR1TUIxwELap/E62G47SRGMhaYgzbANK04Obcqg9xoIj5upz14N8BFgqZyCWmYv2SJdUV5uXIwGITUwr3GgN9vCV5wstxF5aHYbLOTE9zo6ulD71DYOtreD1lCXAHDstDU2Q/TEkhPctDccHlUJAUUqJ9EoEkyVGMACA4gbFjxP0RagNOPIK8N04ROH4qQaMKSDcDUY1RfmsP+wVXsk+MHD6JJUfA5byijXzEWAdpWXkOW5brB1oZf1vu0oE2x5GEBTsAOw67yKSAGhkKwZ07AWI+GZL0N+4/2YHg4Bl+Thf6YwBBbRY24P/omQbsHiuaCIzYEk7ejuklfskQ1ZTXJi3Cg3aW6E+rIg36Gt3AlgyB5TtqQ3W43SAjjxo0TTqezgw1m8WGl6nDttxTtkEOY0A19Dod9quzwQIdk1ub7Jc3lRdSZjNYj9WDOIzAcBiVBJ0d+WDcgIAABkBA0mRYNMu9ghHX6My3FaVOaICv/rvrHBuj3cva5lMS+OHLkyL19fX0mrxEKhSypsbExTrC1tVXmtyC4Nj8FgPeU5c3HT6r/va+pMZJZXGmEg7c7/GP7CqaeK4cM2XQOnTS0kwfgZF6mJzpRlJYYzynKAqoDRUZcQS/Twq3ANIyYSeVkBouRxupEp1aWiv56qJqN4V3OtqA4v+Nnls6NmUIe8fcVEJQWZG8cPnwYZP5HfpPdTHlnpI/LviU9J3+9GBwQabMXPSqCvS9xpXjgZE/4R7IZlf1eu5GXniBa2jgqiBS3A602BUORGBCf65g6rIVihqyzJll40a7Iq7yKfGKIXwrhlHxIhn47+6yiWoebm5ufBZGUlGTE13lCgOA2Erm5uaK3t1ciWYPMZ3ADs50vcuXTz7aEtHusGnMUaaFIyBSmoqqTYjHj3xRZmpWe7ER7T9BgPkk2RRLv7m9BJKpbkpDMsN0rSTFmcDS0G7J6B3XcFY1EpcSMHM3pTwlZhj6T0dvMgaEyepfZuIWQCYbZYBEX7htwcyTmzp0b313RwOsVI1+5tBZ+4eazxJxzZmq3/vg6VV89Rhx9ZBZunlVat/qSyUeuqMq2bpxZZNx3aaVekOzUszyqkZPktrJz8o5nj/H9oBgQlQCKU31qRXGRVsmNOqNUwn5O0K9Fcdadc845mDZtmgxAxL9R/zcoK2pra4WmaYKN5QULFoAkHx/daLfQYQ0NldVTlHm1Fdr1M0uUjXddjH2/XuSYW5K+aum5xQPrr5lm5focVppbDeb4nWuz8ou92UUTkZfqV0ryszX6OXVKMeMUOe7uXgEgrVmzRiLEvHnzBIG/C27MBYkiIyNDzJkzR166dCmYtE+MkhxmeXtFebk8uawUU6smSxdPr7BfN7dafW7pbKz//rSM+6896+lx6Qkv2oE8EIW5uUpJYZ69vLhAKi8tie816GMF9zyhUXKvAlDeeOMNwdlEYlrB5XIJ/CPwJcFjCowfPz5O8qabbgJPA+497SBoD51fwg2OOrGsDNMrKsQWJtxmSWCnwLy9Ahe9y/o7vDeTz7jHBU8lbHy/ju/uO+2U4nEA8j333CP5fD5pzJgxoDgC3wYk+A3J0bMTME/OZWeHRtUcsa9Y/0V1ZeX3cqtrfA+m+2zbBVo3Af0TJJFQfNZZiXx2Dtv9itZw6tCJp2RHeUpRN336dEyZMkX2+/3S2LFjT20zvz24u4qflfBlQbUUEIqiOKjwT5ibhxguiwRNngGaVeycZ4LX7AG+3gJ0TmGd54Mjz+JtRhQjsSYuBisAJIDgqZfi8XhEQkICSFLgTEDZBacc8fzzz4O5KM+YMUMCQdiYp2dze/hQaUnJ1tKysmBpTs7Sz0nwA6BrcmHhYoZ/oHjChB2MxKP0cwEAJwiqJvG9U6e04Ool8F3ACTM+R7ITJCYmCqqhLF68WFx55ZWn54z9+4B7B+IhHp4EpACwMcnih0Fr167FqlWrBFNE4UDgF5U3PhAJ/L+B4Y2HneEAOxBMcpmdKw9yIP2XLOMJQNsFnHwPCNUASfMlCStqa+WpU6cqPK0aISYYUnBREAT+qWAHYF6CEDdIkrhV0+RjAP4CfLQBaGIDGcQUQAIg3nrrrTPu638AMdkGa+XVGZ8AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjQtMDYtMDVUMDU6MjI6MzYrMDA6MDAbUcvBAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDI0LTA2LTA1VDA1OjIyOjM2KzAwOjAwagxzfQAAAABJRU5ErkJggg==)](https://modrinth.com/mod/pick-block-pro/versions)
[![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/inventory_control_tweaks?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues)
[![localized: Percentage](https://badges.crowdin.net/inventory-control-tweaks/localized.svg)](https://crwd.in/inventory-control-tweaks)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/inventory-control-tweaks?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/inventory-control-tweaks/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/421375?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/inventory-control-tweaks/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Tweaks to inventory controls including offhand shift-clicking and dragging out of inventories.

Works client-side* and in single player.

*details in *Pick block tweaks* below

---

#### Current features:

<details>

<summary>Armor swap</summary>

Quickly swap held and worn armor.  
Minecraft 1.19.4 added this feature to vanilla;
Inventory Control Tweaks simply adds configurability on this and later versions.

![Using armor swap to change from diamond armor to netherite armor.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/armor_swap.gif)
![Using armor swap to swap from a chest plate to an elytra after jumping from a high place.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/elytra_swap.gif)
![Using armor swap to equip a golden helmet just in time to avoid being attacked by a piglin.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/piglin_gold_swap.gif)

</details>

<details>

<summary>Pick block tweaks</summary>

The following features alter pick behavior (middle-clicking blocks and entities).

On Minecraft 1.21.4+, if you're in creative and holding control,
these features only work if the mod is installed on the server.  
If it's not, picking will have vanilla behavior.

If you're not in creative and holding control, these features will work even if the mod is not installed on the server.  
These features always work on Minecraft 1.21.3 and below, and they always work in single player.

Pick fills held stack and Pick never changes slot:

![Picking block on a flower that's already in hand, other flowers in inventory fill the stack that's held.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/pick_block_fills_stack.gif)
![Picking block on a flower with an empty hand, a flower in another slot in the inventory moves to the hand.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/pick_block_never_changes_slots.gif)

</details>

<details>

<summary>Drag item tweaks</summary>

Drag single, matching, or all stacks across or out of inventories.

![Dragging a gold axe back and forth between the player inventory and a chest,
other golden axes move back and forth too.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_across_ignore_nbt.gif)  
![Dragging rotten flesh out of inventory to throw it into fire.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_single.gif)

![Dragging rotten flesh out of inventory, all rotten flesh is thrown with it into fire.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_matching.gif)
![Dragging a golden axes out of inventory, all golden exes are thrown out with it: ignoring components is enabled.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/drag_out_ignore_nbt.gif)

</details>

<details>

<summary>Offhand tweaks</summary>

Shift click to move food or other items to or just to fill the offhand.

![Shift clicking on steak moves it in and out of the offhand, shift clicking on rotten flesh just moves it to the hotbar (vanilla behavior): steak is food, rotten flesh is food but has negative effects.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/shift_click_food.gif)
![Shift clicking on firework rockets and a totem of undying moves them in and out of the offhand: they are configured to be offhand-preferred.](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/raw/master/media/offhand_preferred.gif)

</details>

---

<details>

<summary>Configuration</summary>

Supports [Cloth Config](https://modrinth.com/mod/cloth-config/versions) and
[Mod Menu](https://modrinth.com/mod/modmenu/versions) for configuration, but neither is required.

Options will use their default values if [Cloth Config](https://modrinth.com/mod/cloth-config/versions) is absent.

If Cloth Config is present, options can be configured either through
[Mod Menu](https://modrinth.com/mod/modmenu/versions) or by editing `config/inventory_control_tweaks.json` in your
instance folder (`.minecraft/` by default for the vanilla launcher).

#### Key binds:
- Drag matching out of inventory modifier; default: `left ctrl`
- Drag all out of inventory modifier; default: `<unbound>`
- Click to swap armor modifier; default: `right shift`
- Drag all stacks across modifier; default: `left alt`

#### Options:

##### General

- Enable armor swapping; default: `true`
  > Right click while holding a piece of armor to swap it with the piece you're currently wearing.

- Equip/swap non-armor wearables; default: `false`
  > Enables right click to equip/swap non-armor items that can be equipped to armor slots, such as mob skulls.

- Armor swap blacklist:; default: `<none>`
  > Using items in this list won't swap them with your current armor.

- Pick block fills stack; default: `true`
  > If you pick-block while you already have the picked block selected, the stack in your hand will be refilled from
your inventory.

- Pick block never changes slot; default: `true`
  > When you pick-block, your selected hotbar slot won't change.  
  Instead, the picked block will always be moved to that slot.  
  <br />
  If Pick Block Pro is installed, this feature has no effect.  
  Use PBP's "Stay in same hotbar slot" under "Inventory instead".

##### Drag item tweaks

- Drag matching stacks across inventories; default: `true`
  > With an external inventory open (chest, dropper, etc. ), click, hold, and drag a stack between it and your
inventory to move all matching stacks.

- Choose bottom row stacks first; default: `true`
  > When you use 'Drag matching stacks across inventories', stacks will be moved from the bottom of inventories first.

- Move hotbar stacks; default: `true`
  > When you use 'Drag matching stacks across inventories', stacks in your hotbar will be included.
  
- Deposit cursor stack; default: `ON_RELEASE_AT_EMPTY`
  > When you use 'Drag matching stacks across inventories', the stack you were dragging will be deposited.

- Drag single stack out of inventory; default: `true`
  > With an inventory open, click, hold, and drag a stack outside the inventory to throw the stack.

- Drag matching stacks out of inventory; default: `WITH_KEY`
  > With an inventory open, click, hold, and drag a stack outside the inventory to throw it and all matching stacks.  
Optionally requires a key to be held which can be configured in Minecraft's controls interface.

- When comparing stacks, ignore components; default: `false`
  > Components stores things like enchantments, damage, and map data.

##### Offhand tweaks

- Shift-click to partial offhand stack; default: `true`
  > If a partial stack of items is in your offhand, shift-clicking another stack of the same type of items will add those items to your offhand.

- Shift-click any food to offhand; default: `true`
  > Shift-clicking any food in your inventory while your offhand is empty will move that food to your offhand.

- Don't shift-click food with negative effects to offhand; default: `true`
  > Excludes food with negative effects from shift-clicking to your offhand.

- List of items that shift-click to offhand:; default:
`minecraft:shield`, `minecraft:firework_rocket`, `minecraft:totem_of_undying`
  > Shift-clicking items in this list while your offhand is empty will move those items to your offhand.

</details>

---

<details>

<summary>Mod compatibility</summary>

- [Pick Block Pro](https://modrinth.com/mod/pick-block-pro/versions):  
  ICT versions 1.3.27 and up integrate with [Pick Block Pro](https://modrinth.com/mod/pick-block-pro/versions)
  1.7.20 and up.  
  "Pick block never changes slot" is superseded by PBP's "Stay in same hotbar slot" under "Inventory";
  use it instead if PBP is installed.  
  ICT's "Pick block fills stack" feature will function normally.  
  Earlier versions of PBP disable ICT's pick block features, but other features function normally.

- [Item Scroller](https://www.curseforge.com/minecraft/mc-mods/item-scroller):  
  Partially incompatible; ICT's shift-click features won't work.  
  There won't be a crash, and other features of both mods will
  still function.  
  This may be dependent on [Item Scroller](https://www.curseforge.com/minecraft/mc-mods/item-scroller) settings.


</details>

---

<details>

<summary>Translations</summary>

[![localized: Percentage](https://badges.crowdin.net/inventory-control-tweaks/localized.svg)](https://crwd.in/inventory-control-tweaks) [![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
Install [SSS Translate](https://modrinth.com/mod/sss-translate) to automatically download new translations.  
You can help translate Inventory Control Tweaks on [Crowdin](https://crwd.in/inventory-control-tweaks).

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however,
so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
