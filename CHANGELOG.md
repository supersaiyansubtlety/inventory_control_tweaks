- 1.6.1 (23 Feb. 2025): Port 1.5.1 to 1.21.4
- 1.5.1 (23 Feb. 2025): Fixed a bug where shift-clicking an offhand-preferred item when your offhand was already full
  of that item attempted swapping instead of falling back to vanilla's shift-click behavior,
  potentially causing glitchy behavior on servers
- 1.6.0 (31 Jan. 2025):
  - Updated for 1.21.4
  - Some features now require server-side installation to work in creative,
  see "Pick block tweaks" in the mod description/README for details
- 1.6.0-a2 (6 Dec. 2024): Increase minimum minecraft version to 1.21.4
- 1.6.0-a1 (6 Dec. 2024):
  - Updated for 1.21.4
  - Experimental alpha version for testing Pick Block Pro compat with new mixins
- 1.5.0 (15 Nov. 2024):
  - Updated for 1.21.2-1.21.3
  - removed "Enable armor swapping" and "Equip/swap non-armor wearables" configs as item equip-/swap-ability is now
  fully customizable with vanilla data packs
  - replaced "Armor swap deny list" with a more general "Prevent using these items" config
  - Note that an armor swap hotkey is still added and allows swapping armor in your inventory
  - Moderate internal changes
- 1.4.1 (4 Sep. 2024): Marked as compatible with 1.21.1
- 1.4.0 (11 Jul. 2024):
  - Updated for 1.21!
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates
  - Minor internal changes
- 1.3.31 (9 May 2024):
  - Marked as compatible with 1.20.6
  - Fixed a bug that prevented shift clicking immediately after clicking to swap armor in inventory
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.3.30 (25 Apr. 2024):
  - Updated for 1.20.5
  - Fixed "Pick block never changes slot" for some cases with multiple matching stacks on the hot bar
  - Fixed "Click to swap armor modifier" for non-hot bar items
  - Fixed log spam related to unbound keys
  - Renamed "When comparing stacks, ignore NBT" to "When comparing stacks, ignore components"
(it will be reset to the default value)
  - Cleaned up code a little
- 1.3.29 (29 Jan. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Minor internal changes
- 1.3.28 (31 Oct. 2023): Updated for 1.20.2
- 1.3.27 (29 Jul. 2023): 
  - Integrates with Pick Block Pro 1.7.20+
  - "Pick block never changes slot" will have no effect if PBP is installed, use PBP's "Stay in same hotbar slot" under "Inventory" instead
  - Thanks to [Sjouwer](https://github.com/Sjouwer) for collaborating and making changes to PBP that make this integration possible 
  - Thanks to [Felix14-v2](https://gitlab.com/Felix14-v2) for suggesting this integration, testing, and contributing translations
- 1.3.26 (27 Jul. 2023):
  - "Deposit cursor stack on release" is now "Deposit cursor stack". Its values have been renamed, and a new 
"IMMEDIATELY_AT_EMPTY" value is available.
  - Added "Move hotbar stacks" (default true, same behavior as previous versions) that when disabled prevents moving 
hotbar items when dragging stacks across inventories. Closes [#25](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues/25).
  - Fixed using offhand item in addition to swapping armor on right click (reported in [CurseForge comments](https://legacy.curseforge.com/minecraft/mc-mods/inventory-control-tweaks#c86)).
- 1.3.25-1 (8 Jul. 2023): Made Mod Menu description and summary translatable
- 1.3.25 (27 Jun. 2023): If "Deposit cursor stack on release" is not disabled, 
the cursor stack will now be deposited even if no other items were moved. 
Closes [#22](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues/22).
- 1.3.24 (21 Jun. 2023): Fixed a crash that occurred when clicking where no slot was present. Fixes [#21](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues/21)
- 1.3.23 (18 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.3.22 (18 Jun. 2023):
  - Fixed armor and offhand items still getting moved when dragging
  - Fixed cursor stack not being deposited in some situations
  - Prevent throwing stacks after items have been moved via drag and no throw key is held (to prevent some accidental throwing)
- 1.3.21 (17 May 2023):
  - Added "Drag all out of inventory modifier" (default unbound) to throw all stacks in an inventory
  - Prevent throwing or moving items worn by the player (armor or in offhand, click item can be thrown even if worn)
- 1.3.20 (21 Mar. 2023): Updated for 1.19.3 and 1.19.4
- 1.3.19 (13 Nov. 2022): Fixed items moving to the wrong slots in creative when using the armor swap modifier key.
- 1.3.18 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.3.17 (28 Jul. 2022): Marked as compatible with 1.19.1
- 1.3.16 (16 Jul. 2022): Fixed a bug where `armorSwapDenyList` and `offhandPreferredItems` wouldn't load custom values until configs were changed.
- 1.3.15 (22 Jun. 2022): Updated for 1.19
- 1.3.14 (18 May 2022):
  
  Cloth Config is now optional

  Now only compatible with 1.18.2

- 1.3.13 (23 Mar. 2022): 
  
  Marked as compatible with 1.18.2
  
  Thanks to [Ruben Bezuidenhout](https://gitlab.com/Rex-22) for this update!
  
- 1.3.12 (12 Dec. 2021): Marked as compatible with 1.18.1
- 1.3.11 (7 Dec. 2021): 
  
  Fixed compatibility with [Don't Drop It!](https://www.curseforge.com/minecraft/mc-mods/dont-drop-it) versions 2.3.6 and up!

- 1.3.10 (1 Dec. 2021): Updated for 1.18!
- 1.3.9 (7 Nov. 2021): 
  - the default keybinding for "Click to swap armor modifier" is now right-shift (was left-shift, which prevented snaking without the help of other mods)
  - [amecs](https://www.curseforge.com/minecraft/mc-mods/amecs) is now recommended. 
  [amecs](https://www.curseforge.com/minecraft/mc-mods/amecs) allows for multiple bindings to the same key, so it'll allow you to sneak even if you bind "Click to swap armor modifier" to left-shift.
- updated dependencies

- 1.3.8 (23 Jul. 2021): 
  No longer requires autoconfig, as it's built into cloth clothConfig now. 
  Thanks to [J. Fronny](https://gitlab.com/JFronny) for this update! 
- 1.3.7 (9 Jul. 2021): Marked as compatible with 1.17.1.
- 1.3.6 (2 Jul. 2021): Updated for 1.17!
- 1.3.5 (6 Jun. 2021): Fixed some issues when shift clicking things in a creative tab
- 1.3.4 (16 Apr. 2021): Two fixes:
  - Fixed strange behavior when dragging items across the player's 2x2 inventory crafting grid
  - Fixed dragging stacks out of inventory in creative with no external inventory open
- 1.3.3 (26 Mar. 2021): Fixed a strange crash that occurred when many other specific mods were present. 
- 1.3.2 (26 Mar. 2021): Fixed shift-clicking to offhand. 
  Another conflict has been reported with [Architectury](https://www.curseforge.com/minecraft/mc-mods/architectury-fabric), but I can't reproduce it. 
  If you have more information please post it in [this issue](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues/6). 
- 1.3.1 (25 Mar. 2021): Hotfix for crash when using this mod with REI (possibly other mods as well).
- 1.3 (23 Mar. 2021): 

  New features:
  - Hold the 'Click to swap armor modifier' key and click armor to swap it while your inventory's open (thanks to [Xpopy](https://www.curseforge.com/members/xpopy/projects) for the idea!)
  - Hold the 'Drag all stacks across modifier' key to move *all* items (not just matching ones) across inventories
  
  Improved 'Deposit cursor stack on release' behaviors.
  
  Organized settings into categories.
  
  Added option to disable translation fetching.
- 1.2.4 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.2.3 (14 Jan. 2021): New feature:
  - "Choose bottom row stacks first": When you use 'Drag matching stacks across inventories', stacks will be moved from the bottom of inventories first.
  Also improved "Deposit cursor stack on release" so that if there's no room to deposit the cursor stack in the new inventory, the stack will be put back in the old inventory. 
  1.16.1 version only: fixed bundling the wrong version of cloth clothConfig
- 1.2.2 (13 Jan. 2021): Fixed some issues where items would shift-click to your offhand when they shouldn't. 
- 1.2.1 (12 Jan. 2021): New (compatibility) feature: 
  - "Armor swap item blacklist", default is empty. 
  
  Fixed armor swapping when armor is in offhand. 
- 1.2 (12 Jan. 2021): New features!
  - "When comparing stacks, ignore NBT", default is No/false
  - "Deposit cursor stack on release", default is 'AT_EMPTY_SLOT'
    This affects what happens when you let go after using the existing "Drag matching stacks across inventories" feature
  - "Drag single stack out of inventory", default is Yes/true
  - "Drag matching stacks out of inventory", default is 'WITH_KEY'
    Configure the (optional) key from Minecraft's controls interface
- 1.1.1 (20 Dec. 2020): Prevent the shift-click features from interfering when the player has an external inventory open, 
  so items that are offhand-preferred can be shift-clicked between different inventories like other items. 
  
  There's a known issue in this and version and 1.1:
  Sometimes when using pick-block in creative, the player's main-hand block will de-sync with the server, so they won't 
  actually be able to place the block they've picked (it will appear to be placed momentarily, but instantly disappear). 
  
  If you've experienced this issue and have some idea of how to reproduce it, please leave a comment or, better yet, 
  create an issue on the [issue tracker](https://gitlab.com/supersaiyansubtlety/inventory_control_tweaks/-/issues), it will be much appreciated!
- 1.1 (8 Dec. 2020): New features! 
  - Pick block fills stack: If you pick-block while you already have the picked block selected, the stack in your hand 
    will be refilled from your inventory. 
  - Pick block never changes slot: If you pick-block while the block you're pointing at is on your hotbar, instead of 
    changing which slot you have selected, that block will be swapped to your hand. 
  - Drag matching stacks across inventories: With an external inventory open (chest, dropper, etc. ), click, hold, and 
    drag a stack between the two inventories to move all matching stacks. 
  
  Added mod icon (visible in Mod Menu's mod list). 
  
  Translations are now handled with [CrowdinTranslate](https://github.com/gbl/CrowdinTranslate), go [here](https://crowdin.com/project/inventory-control-tweaks) if you'd like to contribute a translation. 
- 1.0.0 (20 Nov. 2020): Initial release version! 
- 0.0.1: Initial version. 
