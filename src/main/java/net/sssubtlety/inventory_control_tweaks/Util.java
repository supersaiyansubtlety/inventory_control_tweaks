package net.sssubtlety.inventory_control_tweaks;

import net.sssubtlety.inventory_control_tweaks.init.Networking;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;

import net.minecraft.block.BlockState;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.Slot;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.component.type.EquippableComponent;
import net.minecraft.util.Hand;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;

import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.IntConsumer;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.LOGGER;
import static net.sssubtlety.inventory_control_tweaks.mixin.accessor.ServerPlayNetworkHandlerAccessor.inventory_control_tweaks$copyBlockEntityDataToStack;

import static net.minecraft.item.ItemStack.itemsAndComponentsMatch;

public final class Util {
    private Util() { }

    public static int getId(Slot slot) {
        return slot.id;
    }

    public static boolean isFull(ItemStack stack) {
        return stack.getCount() >= stack.getMaxCount();
    }

    // indices: 9-35, 0-8
    // ids: 9-35, 36-44
    public static int getPlayerSlotId(int slotIndex) {
        return slotIndex < PlayerInventory.getHotbarSize() ? slotIndex + PlayerInventory.MAIN_SIZE : slotIndex;
    }

    public static int getPlayerSlotIndex(int slotId) {
        return slotId >= PlayerInventory.MAIN_SIZE ? slotId - PlayerInventory.MAIN_SIZE : slotId;
    }

    public static Text replace(Text text, String regex, String replacement) {
        String string = text.getString();
        string = string.replaceAll(regex, replacement);
        return Text.literal(string).setStyle(text.getStyle());
    }

    public static boolean isModLoaded(String id, String versionPredicate) {
        return FabricLoader.getInstance().getModContainer(id)
            .filter(modContainer -> {
                try {
                    return VersionPredicate.parse(versionPredicate)
                        .test(modContainer.getMetadata().getVersion());
                } catch (VersionParsingException e) {
                    LOGGER.error("Failed to parse version predicate", e);
                    return false;
                }
            })
            .isPresent();
    }

    public static boolean isModLoaded(String id) {
        return FabricLoader.getInstance().isModLoaded(id);
    }

    public static Optional<EquipmentSlot> getEquippableSlot(PlayerEntity player, ItemStack stack) {
        final EquipmentSlot equipmentSlot = player.getPreferredEquipmentSlot(stack);
        if (
            equipmentSlot.getType() != EquipmentSlot.Type.HUMANOID_ARMOR ||
            player.getEquippedStack(equipmentSlot).isEmpty()
        ) {
            return Optional.empty();
        }

        // don't check equippable.swappable() because a hotkey is being held which indicates intent to swap
        final EquippableComponent equippable = stack.get(DataComponentTypes.EQUIPPABLE);
        if (equippable == null || !equippable.allows(player.getType())) {
            return Optional.empty();
        }

        return Optional.of(equipmentSlot);
    }

    /**
     * @return the picked stack, or {@code null} if pick behavior shouldn't change
     */
    @Nullable
    public static ItemStack tryReplacePick(
        ItemStack pickedStack, PlayerEntity player, boolean pickFillsStack, boolean pickNeverChangesSlot,
        BiConsumer<Integer, Integer> combineSlots, IntConsumer fillCreativeSlot, IntConsumer swapSlotWithSelected
    ) {
        if (pickedStack.isEmpty() || !(pickFillsStack || pickNeverChangesSlot)) {
            return null;
        }

        final PlayerInventory inventory = player.getInventory();

        final ItemStack mainHandStack = inventory.getMainHandStack();

        if (itemsAndComponentsMatch(pickedStack, mainHandStack)) {
            if (pickFillsStack && !isFull(mainHandStack)) {
                final int initialCount = mainHandStack.getCount();
                // combine each other matching stacks until mainHandStack is full
                final int invSize = inventory.main.size();
                for (int i = 0; i < invSize; i++) {
                    if (i == inventory.selectedSlot) {
                        continue;
                    }

                    if (itemsAndComponentsMatch(inventory.main.get(i), mainHandStack)) {
                        combineSlots.accept(i, inventory.selectedSlot);

                        if (isFull(mainHandStack)) {
                            break;
                        }
                    }
                }

                if (mainHandStack.getCount() == initialCount) {
                    if (player.isCreative()) {
                        fillCreativeSlot.accept(inventory.selectedSlot);

                        return mainHandStack;
                    }
                } else {
                    return mainHandStack;
                }
            }
        } else if (pickNeverChangesSlot) {
            final int invSize = inventory.main.size();

            for (int i = 0; i < invSize; i++) {
                if (i == inventory.selectedSlot) {
                    continue;
                }

                if (itemsAndComponentsMatch(inventory.main.get(i), pickedStack)) {
                    // swap selected slot with matching stack instead
                    swapSlotWithSelected.accept(i);

                    return player.getStackInHand(Hand.MAIN_HAND);
                }
            }
        }

        return null;
    }

    /**
     * See also {@code ClientUtil::tryReplaceClientEntityPick}
     *
     * @return the picked stack, or {@code null} if pick behavior shouldn't change
     */
    @Nullable
    public static ItemStack tryReplaceServerBlockPick(
        ServerPlayerEntity player, BlockPos pos, BlockState state, boolean includeData
    ) {
        if (!includeData || !player.isCreative()) {
            return null;
        }

        // AbstractBlockState::method_65171 is getPickStack
        final ItemStack pickedStack = state.method_65171(player.getServerWorld(), pos, true);
        inventory_control_tweaks$copyBlockEntityDataToStack(state, player.getServerWorld(), pos, pickedStack);
        return tryReplaceServerPick(
            pickedStack,
            player, Networking.pickFillsStack(player), Networking.pickNeverChangesSlot(player)
        );
    }

    /**
     * See also {@code ClientUtil::tryReplaceClientEntityPick}
     *
     * @return the picked stack, or {@code null} if pick behavior shouldn't change
     */
    @Nullable
    public static ItemStack tryReplaceServerEntityPick(
        ServerPlayerEntity player, Entity entity, boolean includeData
    ) {
        if (!includeData || !player.isCreative()) {
            return null;
        }

        return tryReplaceServerPick(
            entity.getPickBlockStack(),
            player, Networking.pickFillsStack(player), Networking.pickNeverChangesSlot(player)
        );
    }

    @Nullable
    private static ItemStack tryReplaceServerPick(
        ItemStack pickedStack, ServerPlayerEntity player,
        boolean pickFillsStack, boolean pickNeverChangesSlot
    ) {
        return tryReplacePick(
            pickedStack, player,
            pickFillsStack, pickNeverChangesSlot,
            (fromIndex, toIndex) -> combineServerSlots(player, fromIndex, toIndex),
            slotIndex -> fillServerCreativeSlot(player, slotIndex),
            slotIndex -> swapServerSlotWithSelected(player, slotIndex)
        );
    }

    // assumes stacks in fromIndex and toIndex match (are combinable) and that the stack in toIndex is not full
    private static void combineServerSlots(ServerPlayerEntity player, int fromIndex, int toIndex) {
        final DefaultedList<ItemStack> mainInventory = player.getInventory().main;

        final ItemStack from = mainInventory.get(fromIndex);
        final ItemStack to = mainInventory.get(toIndex);

        final int available = from.getCount();
        final int space = to.getMaxCount() - to.getCount();

        final int transferAmount = Math.min(space, available);
        to.increment(transferAmount);
        from.decrement(transferAmount);
    }

    private static void fillServerCreativeSlot(ServerPlayerEntity player, int slotIndex) {
        final ItemStack stack = player.getInventory().main.get(slotIndex);
        stack.setCount(stack.getMaxCount());
    }

    private static void swapServerSlotWithSelected(ServerPlayerEntity player, int slotIndex) {
        final PlayerInventory inventory = player.getInventory();

        final int selectedSlot = inventory.selectedSlot;
        final DefaultedList<ItemStack> mainInventory = inventory.main;

        final ItemStack stack = mainInventory.get(slotIndex);
        mainInventory.set(slotIndex, mainInventory.get(selectedSlot));
        mainInventory.set(selectedSlot, stack);
    }
}
