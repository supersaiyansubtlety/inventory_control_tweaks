package net.sssubtlety.inventory_control_tweaks.mixin.accessor;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(ServerPlayNetworkHandler.class)
public interface ServerPlayNetworkHandlerAccessor {
    @Invoker("method_65099")
    static void inventory_control_tweaks$copyBlockEntityDataToStack(
        BlockState state, ServerWorld world, BlockPos pos, ItemStack stack
    ) {
        throw new IllegalStateException("dummy method called");
    }
}
