package net.sssubtlety.inventory_control_tweaks.init;

import net.sssubtlety.inventory_control_tweaks.Util;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.PlayerPickItemEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;

public final class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ServerPlayConnectionEvents.JOIN.register((Networking::onServerJoin));

        ServerPlayConnectionEvents.DISCONNECT
            .register(((handler, server) -> Networking.onServerDisconnect(handler.player)));

        ServerPlayNetworking.registerGlobalReceiver(
            Networking.ClientSettingsPayload.ID,
            (payload, context) -> Networking.onSettingsReceived(payload, context.player())
        );

        PlayerPickItemEvents.BLOCK.register(Util::tryReplaceServerBlockPick);

        PlayerPickItemEvents.ENTITY.register(Util::tryReplaceServerEntityPick);
    }
}
