package net.sssubtlety.inventory_control_tweaks.init;

import net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks;

import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;

import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.listener.ServerCommonPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.c2s.common.CustomPayloadC2SPacket;
import net.minecraft.network.packet.payload.CustomPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.idOf;

public final class Networking {
    private static final Set<ServerPlayerEntity> PICK_NEVER_CHANGES_SLOT_PLAYERS = new HashSet<>();

    private static final Set<ServerPlayerEntity> PICK_FILLS_STACK_PLAYERS = new HashSet<>();

    private static boolean serverHasICT;

    public static boolean pickFillsStack(ServerPlayerEntity player) {
        return PICK_FILLS_STACK_PLAYERS.contains(player);
    }

    public static boolean pickNeverChangesSlot(ServerPlayerEntity player) {
        return PICK_NEVER_CHANGES_SLOT_PLAYERS.contains(player);
    }

    static void onSettingsRequest(
        boolean pickFillsStack, boolean pickNeverChangesSlot,
        Consumer<CustomPayload> sendPayload
    ) {
        serverHasICT = true;

        if (pickFillsStack || pickNeverChangesSlot) {
            sendPayload.accept(new Networking.ClientSettingsPayload(pickFillsStack, pickNeverChangesSlot));
        }
    }

    static void onClientDisconnect() {
        serverHasICT = false;
    }

    static void onServerJoin(ServerPlayNetworkHandler handler, PacketSender sender, MinecraftServer server) {
        sender.sendPacket(RequestClientSettingsPayload.INSTANCE);
    }

    static void onServerDisconnect(ServerPlayerEntity player) {
        PICK_FILLS_STACK_PLAYERS.remove(player);
        PICK_NEVER_CHANGES_SLOT_PLAYERS.remove(player);
    }

    public static void onSettingsSaved(
        boolean pickFillsStack, boolean pickNeverChangesSlot,
        Consumer<Packet<?>> sendPacket
    ) {
        if (serverHasICT) {
            sendPacket.accept(ClientSettingsPayload.packetOf(pickFillsStack, pickNeverChangesSlot));
        }
    }

    static void onSettingsReceived(ClientSettingsPayload payload, ServerPlayerEntity player) {
        if (payload.pickFillsStack()) {
            PICK_FILLS_STACK_PLAYERS.add(player);
        } else {
            PICK_FILLS_STACK_PLAYERS.remove(player);
        }

        if (payload.pickNeverChangesSlot()) {
            PICK_NEVER_CHANGES_SLOT_PLAYERS.add(player);
        } else {
            PICK_NEVER_CHANGES_SLOT_PLAYERS.remove(player);
        }
    }

    record ClientSettingsPayload(
        boolean pickFillsStack,
        boolean pickNeverChangesSlot
    ) implements CustomPayload {
        public static final Id<ClientSettingsPayload> ID = new Id<>(InventoryControlTweaks.idOf("client_settings"));

        private static final PacketCodec<PacketByteBuf, ClientSettingsPayload> CODEC = PacketCodec.tuple(
            PacketCodecs.BOOL, ClientSettingsPayload::pickFillsStack,
            PacketCodecs.BOOL, ClientSettingsPayload::pickNeverChangesSlot,
            ClientSettingsPayload::new
        );

        static {
            PayloadTypeRegistry.playC2S().register(ID, CODEC);
        }

        public static Packet<ServerCommonPacketListener> packetOf(
            boolean pickFillsStack, boolean pickNeverChangesSlot
        ) {
            return new CustomPayloadC2SPacket(new ClientSettingsPayload(pickFillsStack, pickNeverChangesSlot));
        }

        @Override
        public Id<ClientSettingsPayload> getId() {
            return ID;
        }
    }

    static final class RequestClientSettingsPayload implements CustomPayload {
        public static final RequestClientSettingsPayload INSTANCE = new RequestClientSettingsPayload();
        public static final Id<RequestClientSettingsPayload> ID = new Id<>(idOf("request_client_settings"));

        private static final PacketCodec<PacketByteBuf, RequestClientSettingsPayload> CODEC =
            PacketCodec.unit(INSTANCE);

        static {
            PayloadTypeRegistry.playS2C().register(ID, CODEC);
        }

        private RequestClientSettingsPayload() { }

        @Override
        public Id<RequestClientSettingsPayload> getId() {
            return ID;
        }
    }
}
