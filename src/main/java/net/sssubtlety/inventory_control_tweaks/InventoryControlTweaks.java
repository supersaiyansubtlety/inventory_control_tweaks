package net.sssubtlety.inventory_control_tweaks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

/*
TODO:
	- prevent drag across depositing to offhand
	- test why armor is dragged with cammie's wearable backpacks
	- add 'swap cursor stack with matching full stacks' option
	- add option to make shift-clicking start with hotbar slot 0 instead of 8
	- pickup item entities from ground to offhand
	- drag out of inventories: throw all
*/
public final class InventoryControlTweaks {
	private InventoryControlTweaks() { }

	public static final String NAMESPACE = "inventory_control_tweaks";

	public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");

	public static final Logger LOGGER = LoggerFactory.getLogger(InventoryControlTweaks.class);

	public static Identifier idOf(String path) {
		return Identifier.of(NAMESPACE, path);
	}
}
