package net.sssubtlety.inventory_control_tweaks.mixin_helper;

public interface HandledScreenMouseClickedMixinAccessor {
    boolean inventory_control_tweaks$didReplaceMouseClickSlot();
}
