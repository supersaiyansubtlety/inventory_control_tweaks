package net.sssubtlety.inventory_control_tweaks.mixin_helper;

import net.minecraft.inventory.Inventory;
import org.jetbrains.annotations.Nullable;

public interface MouseDraggedMixinAccessor {
    @Nullable Inventory inventory_control_tweaks$getSource();
    @Nullable Inventory inventory_control_tweaks$getDestination();
    boolean inventory_control_tweaks$hasMovedStacks();
    boolean inventory_control_tweaks$hasCursorCrossedInventories();
    void inventory_control_tweaks$endMove();
}
