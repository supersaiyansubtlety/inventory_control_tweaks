package net.sssubtlety.inventory_control_tweaks.init;

import net.sssubtlety.inventory_control_tweaks.config.ConfigManager;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.pickFillsStack;
import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.pickNeverChangesSlot;

public final class ClientInit implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        ConfigManager.init();

        ClientPlayNetworking.registerGlobalReceiver(
            Networking.RequestClientSettingsPayload.ID,
            (payload, context) -> Networking.onSettingsRequest(
                pickFillsStack(), pickNeverChangesSlot(),
                context.responseSender()::sendPacket
            )
        );

        ClientPlayConnectionEvents.DISCONNECT.register((handler, client) -> Networking.onClientDisconnect());
    }
}
