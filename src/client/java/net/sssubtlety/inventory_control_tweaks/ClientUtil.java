package net.sssubtlety.inventory_control_tweaks;

import net.sssubtlety.inventory_control_tweaks.config.ConfigManager;
import net.sssubtlety.inventory_control_tweaks.mixin.accessor.LivingEntityAccessor;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.Nullable;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.option.KeyBind;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.math.BlockPos;

import com.mojang.blaze3d.platform.InputUtil;

import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Function;

import static net.sssubtlety.inventory_control_tweaks.Util.getPlayerSlotId;
import static net.sssubtlety.inventory_control_tweaks.Util.isModLoaded;
import static net.sssubtlety.inventory_control_tweaks.Util.tryReplacePick;
import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.moveHotbarStacks;
import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.pickFillsStack;
import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.pickNeverChangesSlot;

import static net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper.getBoundKeyOf;

import static net.minecraft.item.ItemStack.itemsAndComponentsMatch;
import static net.minecraft.item.ItemStack.itemsMatch;

public final class ClientUtil {
    private ClientUtil() { }

    private static void swapClientSlotWithSelected(
        ClientPlayerEntity player, ClientPlayerInteractionManager interactionManager, int slotId
    ) {
        interactionManager.clickSlot(
            player.playerScreenHandler.syncId, slotId, player.getInventory().selectedSlot,
            SlotActionType.SWAP, player
        );
    }

    public static boolean isKeyPressed(KeyBind key, MinecraftClient client) {
        final int boundKeyCode = getBoundKeyOf(key).getKeyCode();
        return boundKeyCode >= 0 && InputUtil.isKeyPressed(client.getWindow().getHandle(), boundKeyCode);
    }

    public static void clickSlotId(int slotId, SlotActionType actionType, ScreenHandler handler) {
        final MinecraftClient client = MinecraftClient.getInstance();
        if (client != null && client.interactionManager != null && client.player != null) {
            client.interactionManager.clickSlot(handler.syncId, slotId, 0, actionType, client.player);
        }
    }

    public static boolean insertInEmptySlot(
        ItemStack stack, @Nullable Inventory destination,
        PlayerInventory playerInventory, ScreenHandler handler
    ) {
        final List<Slot> orderedSlots = destination == playerInventory ?
            Lists.reverse(handler.slots) : handler.slots;
        for (final Slot slot : orderedSlots) {
            if (
                slot.inventory == destination &&
                (slot.inventory != playerInventory || slot.getIndex() != PlayerInventory.OFF_HAND_SLOT)
            ) {
                final ItemStack slotStack = slot.getStack();
                if (slotStack.isEmpty() || (!Util.isFull(stack) && itemsAndComponentsMatch(slotStack, stack))) {
                    clickSlotId(slot.id, SlotActionType.PICKUP, handler);
                    if (stack.isEmpty()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static boolean trySwapEquipment(ClientPlayerEntity player, ItemStack stack, int sourceSlotIndex) {
        final ClientPlayerInteractionManager interactionManager = MinecraftClient.getInstance().interactionManager;
        if (interactionManager == null) {
            return false;
        }

        final Optional<Integer> equipmentSlotId = Util.getEquippableSlot(player, stack)
            .map(EquipmentSlot::getEntitySlotId);

        if (equipmentSlotId.isPresent()) {
            combineOrSwapClientSlots(
                player, interactionManager,
                PlayerInventory.getHotbarSize() - 1 - equipmentSlotId.orElseThrow(),
                getPlayerSlotId(sourceSlotIndex)
            );

            ((LivingEntityAccessor) player).inventory_control_tweaks$callProcessEquippedStack(stack);

            return true;
        } else {
            return false;
        }
    }

    public static void depositRemainder(
        ItemStack cursorStack, @Nullable Inventory destination, @Nullable Inventory source,
        PlayerInventory playerInventory, ScreenHandler handler
    ) {
        if (!insertInEmptySlot(cursorStack, destination, playerInventory, handler)) {
            // cursor stack didn't fit
            if (destination != source) {
                // put the rest back in source
                insertInEmptySlot(cursorStack, source, playerInventory, handler);
            }
        }
    }

    /**
     * @return {@code true} iff the passed {@code slot} changes as a result of clicking
     */
    public static boolean trySlotClick(
        ClientPlayerEntity player, @Nullable Inventory inventory, ClientPlayerInteractionManager interactionManager,
        int button, SlotActionType action, ScreenHandler handler, Slot slot,
        Function<Slot, Integer> idGetter, BiPredicate<ClientPlayerEntity, ItemStack> shouldClick
    ) {
        if (
            slot != null &&
            slot.canTakeItems(player) &&
            slot.hasStack() &&
            slot.inventory == inventory
        ) {
            final ItemStack curStack = slot.getStack();
            if (shouldClick.test(player, curStack) && ScreenHandler.canInsertItemIntoSlot(slot, curStack, true)) {
                final ItemStack oldStack = curStack.copy();
                interactionManager.clickSlot(handler.syncId, idGetter.apply(slot), button, action, player);
                return !oldStack.equals(slot.getStack());
            }
        }

        return false;
    }

    public static boolean notWorn(ClientPlayerEntity player, ItemStack stack) {
        for (final ItemStack offhandStack : player.getInventory().offHand) {
            if (stack == offhandStack) {
                return false;
            }
        }

        for (final ItemStack armorStack : player.getInventory().armor) {
            if (stack == armorStack) {
                return false;
            }
        }

        return true;
    }

    // if stacks can combine, they combine at toSlotId, leaving remainder at fromSlotId
    // otherwise, they swap
    public static void combineOrSwapClientSlots(
        ClientPlayerEntity player,
        ClientPlayerInteractionManager interactionManager,
        int fromSlotId, int toSlotId
    ) {
        interactionManager.clickSlot(player.playerScreenHandler.syncId, fromSlotId, 0, SlotActionType.PICKUP, player);
        interactionManager.clickSlot(player.playerScreenHandler.syncId, toSlotId, 0, SlotActionType.PICKUP, player);
        interactionManager.clickSlot(player.playerScreenHandler.syncId, fromSlotId, 0, SlotActionType.PICKUP, player);
    }

    private static boolean notWornAndMatches(ClientPlayerEntity player, ItemStack stack, ItemStack referenceStack) {
        final boolean stacksMatch = ConfigManager.ignoreStackComponents() ?
            itemsMatch(referenceStack, stack)
            : itemsAndComponentsMatch(referenceStack, stack);
        return stacksMatch && notWorn(player, stack);
    }

    /**
     * @return {@code true} iff slots change as a result of clicking
     */
    @SuppressWarnings("UnusedReturnValue")
    public static boolean clickAllSlots(
        List<Slot> slots, ClientPlayerEntity player, @Nullable Inventory inventory,
        ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler,
        Function<Slot, Integer> idGetter
    ) {
        return clickEachSlot(
            slots, player, inventory,
            interactionManager, button, action, handler,
            idGetter, null
        );
    }

    /**
     * @return {@code true} iff slots change as a result of clicking
     */
    public static boolean clickEachSlot(
        List<Slot> slots, ClientPlayerEntity player, @Nullable Inventory inventory,
        ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler,
        Function<Slot, Integer> idGetter, @Nullable ItemStack referenceStack
    ) {
        return clickEachSlotImpl(
            slots, player, inventory,
            interactionManager, button, action, handler, idGetter,
            referenceStack == null ? ClientUtil::notWorn
                : (clientPlayer, stack) -> notWornAndMatches(clientPlayer, stack, referenceStack)
        );
    }

    /**
     * @return {@code true} iff slots change as a result of clicking
     */
    private static boolean clickEachSlotImpl(
        List<Slot> slots, ClientPlayerEntity player, @Nullable Inventory inventory,
        ClientPlayerInteractionManager interactionManager, int button, SlotActionType action, ScreenHandler handler,
        Function<Slot, Integer> idGetter, BiPredicate<ClientPlayerEntity, ItemStack> shouldClick
    ) {
        boolean slotsChanged = false;
        final boolean preventMovingHotbarStacks = !moveHotbarStacks() && inventory == player.getInventory();
        for (final Slot slot : slots) {
            if (preventMovingHotbarStacks && PlayerInventory.isValidHotbarIndex(slot.getIndex())) {
                continue;
            }

            slotsChanged |= trySlotClick(
                player, inventory, interactionManager, button, action, handler, slot, idGetter, shouldClick
            );
        }

        return slotsChanged;
    }

    /**
     * @return {@code true} <i>iff</i> original pick item behavior should be allowed
     */
    public static boolean tryReplaceClientBlockPick(
        MinecraftClient client, ClientPlayerInteractionManager interactionManager, BlockPos pos, boolean includeData
    ) {
        final ClientPlayerEntity player = client.player;
        // let servers handle creative data-picking
        if (player == null || (includeData && player.isCreative())) {
            return true;
        }

        // AbstractBlockState::method_65171 is getPickStack
        final ItemStack pickedStack = player.clientWorld.getBlockState(pos)
            .method_65171(player.clientWorld, pos, false);
        return tryReplaceClientPick(pickedStack, player, interactionManager);
    }

    /**
     * @return {@code true} <i>iff</i> original pick item behavior should be allowed
     */
    public static boolean tryReplaceClientEntityPick(
        MinecraftClient client, ClientPlayerInteractionManager interactionManager, Entity entity, boolean includeData
    ) {
        final ClientPlayerEntity player = client.player;
        // let servers handle creative data-picking
        if (player == null || (includeData && player.isCreative())) {
            return true;
        }

        final ItemStack pickBlockStack = entity.getPickBlockStack();

        if (pickBlockStack == null) {
            return true;
        }

        return tryReplaceClientPick(pickBlockStack, player, interactionManager);
    }

    private static boolean tryReplaceClientPick(
        ItemStack pickedStack, ClientPlayerEntity player, ClientPlayerInteractionManager interactionManager
    ) {
        return null == tryReplacePick(
            pickedStack, player,
            pickFillsStack(), pickNeverChangesSlot(),
            (fromIndex, toIndex) -> combineOrSwapClientSlots(
                player, interactionManager,
                getPlayerSlotId(fromIndex), getPlayerSlotId(toIndex)
            ),
            slotIndex -> fillClientCreativeSlot(player, interactionManager, getPlayerSlotId(slotIndex)),
            slotIndex -> swapClientSlotWithSelected(player, interactionManager, getPlayerSlotId(slotIndex))
        );
    }

    private static void fillClientCreativeSlot(
        ClientPlayerEntity player, ClientPlayerInteractionManager interactionManager, int slotId
    ) {
        interactionManager.clickSlot(
            player.playerScreenHandler.syncId, slotId, 2,
            SlotActionType.CLONE, player
        );

        interactionManager.clickCreativeStack(ItemStack.EMPTY, slotId);

        interactionManager.clickSlot(
            player.playerScreenHandler.syncId, slotId, 0,
            SlotActionType.PICKUP, player
        );
    }
}
