package net.sssubtlety.inventory_control_tweaks.config;

import org.jetbrains.annotations.UnmodifiableView;

import net.minecraft.client.gui.screen.Screen;

import java.util.List;
import java.util.function.Function;

public interface Config {
    @UnmodifiableView
    List<String> preventUsingItems();

    boolean pickFillsStack();

    boolean pickNeverChangesSlot();

    // drag item tweaks
    boolean dragMatchingStacksAcrossInventories();

    boolean chooseBottomRowStacksFirst();

    boolean moveHotbarStacks();

    DepositType depositCursorStack();

    boolean dragSingleStackOutOfInventory();

    Keyable dragMatchingStacksOutOfInventory();

    boolean ignoreStackComponents();

    // offhand tweaks
    boolean shiftClickToOffhandStack();

    boolean allFoodIsOffhandPreferred();

    boolean excludeFoodWithNegativeEffects();

    @UnmodifiableView
    List<String> offhandPreferredItems();

    Function<Screen, ? extends Screen> screenFactory();

    enum DepositType {
        DISABLED,
        AT_RELEASE_SLOT,
        ON_RELEASE_AT_EMPTY,
        IMMEDIATELY_AT_EMPTY
    }

    @SuppressWarnings("unused")
    enum Keyable {
        DISABLED,
        NO_KEY,
        WITH_KEY
    }
}
