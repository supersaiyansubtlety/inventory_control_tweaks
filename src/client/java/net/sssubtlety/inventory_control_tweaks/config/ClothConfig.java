package net.sssubtlety.inventory_control_tweaks.config;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import org.jetbrains.annotations.UnmodifiableView;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ActionResult;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class ClothConfig implements ConfigData, Config {
    public static ClothConfig register(
        Function<ClothConfig, ActionResult> onLoad, Function<ClothConfig, ActionResult> onSave
    ) {
        final ConfigHolder<ClothConfig> holder = AutoConfig.register(ClothConfig.class, GsonConfigSerializer::new);
        final ClothConfig config = holder.getConfig();

        onLoad.apply(config);
        holder.registerLoadListener((loadedHolder, loadedConfig) -> onLoad.apply(loadedConfig));
        holder.registerSaveListener((savedHolder, savedConfig) -> onSave.apply(savedConfig));

        return config;
    }

    @ConfigEntry.Gui.Tooltip
    @UnmodifiableView
    public final List<String> prevent_using_items = DefaultConfig.INSTANCE.preventUsingItems();

    @ConfigEntry.Gui.Tooltip
    public boolean enable_pick_fills_stack = DefaultConfig.INSTANCE.pickFillsStack();

    @ConfigEntry.Gui.Tooltip
    public boolean enable_pick_never_changes_slot = DefaultConfig.INSTANCE.pickNeverChangesSlot();

    // drag_item_tweaks
    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean drag_matching_stacks_across_inventories = DefaultConfig.INSTANCE.dragMatchingStacksAcrossInventories();

    @ConfigEntry.Gui.PrefixText

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean choose_bottom_row_stacks_first = DefaultConfig.INSTANCE.chooseBottomRowStacksFirst();

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean move_hotbar_stacks = DefaultConfig.INSTANCE.moveHotbarStacks();

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public Config.DepositType deposit_cursor_stack = DefaultConfig.INSTANCE.depositCursorStack();

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean drag_single_stack_out_of_inventory = DefaultConfig.INSTANCE.dragSingleStackOutOfInventory();

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public Config.Keyable drag_matching_stacks_out_of_inventory = DefaultConfig.INSTANCE.dragMatchingStacksOutOfInventory();

    @ConfigEntry.Category("drag_item_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean ignore_stack_components = DefaultConfig.INSTANCE.ignoreStackComponents();

    // offhand tweaks
    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean enable_shift_click_to_offhand_stack = DefaultConfig.INSTANCE.shiftClickToOffhandStack();

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean all_food_is_offhand_preferred = DefaultConfig.INSTANCE.allFoodIsOffhandPreferred();

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    public boolean exclude_food_with_negative_effects = DefaultConfig.INSTANCE.excludeFoodWithNegativeEffects();

    @ConfigEntry.Category("offhand_tweaks")
    @ConfigEntry.Gui.Tooltip
    @UnmodifiableView
    public final List<String> offhand_preferred_items = DefaultConfig.INSTANCE.offhandPreferredItems();

    @Override
    public List<String> preventUsingItems() { return this.prevent_using_items; }

    @Override
    public boolean pickFillsStack() { return this.enable_pick_fills_stack; }

    @Override
    public boolean pickNeverChangesSlot() { return this.enable_pick_never_changes_slot; }

    // drag item tweaks
    @Override
    public boolean dragMatchingStacksAcrossInventories() { return this.drag_matching_stacks_across_inventories; }

    @Override
    public boolean chooseBottomRowStacksFirst() { return this.choose_bottom_row_stacks_first; }

    @Override
    public boolean moveHotbarStacks() { return this.move_hotbar_stacks; }

    @Override
    public DepositType depositCursorStack() { return this.deposit_cursor_stack; }

    @Override
    public boolean dragSingleStackOutOfInventory() { return this.drag_single_stack_out_of_inventory; }

    @Override
    public Keyable dragMatchingStacksOutOfInventory() { return this.drag_matching_stacks_out_of_inventory; }

    @Override
    public boolean ignoreStackComponents() { return this.ignore_stack_components; }

    // offhand tweaks
    @Override
    public boolean shiftClickToOffhandStack() { return this.enable_shift_click_to_offhand_stack; }

    @Override
    public boolean allFoodIsOffhandPreferred() { return this.all_food_is_offhand_preferred; }

    @Override
    public boolean excludeFoodWithNegativeEffects() { return this.exclude_food_with_negative_effects; }

    @Override
    public List<String> offhandPreferredItems() {
        //noinspection RedundantUnmodifiable
        return Collections.unmodifiableList(this.offhand_preferred_items);
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return parent -> AutoConfig.getConfigScreen(ClothConfig.class, parent).get();
    }
}
