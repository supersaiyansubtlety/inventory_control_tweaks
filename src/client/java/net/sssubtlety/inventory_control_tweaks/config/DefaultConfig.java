package net.sssubtlety.inventory_control_tweaks.config;

import net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks;
import net.sssubtlety.inventory_control_tweaks.Util;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.function.Function;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.NAMESPACE;

public final class DefaultConfig implements Config {
    public static final DefaultConfig INSTANCE = new DefaultConfig();

    private static final ImmutableList<String> PREVENT_USING_ITEMS = ImmutableList.of();

    private static final ImmutableList<String> OFFHAND_PREFERRED_ITEMS = ImmutableList.of(
        Registries.ITEM.getId(Items.SHIELD).toString(),
        Registries.ITEM.getId(Items.FIREWORK_ROCKET).toString(),
        Registries.ITEM.getId(Items.TOTEM_OF_UNDYING).toString()
    );

    private DefaultConfig() {}

    @Override
    public ImmutableList<String> preventUsingItems() {return PREVENT_USING_ITEMS;}

    @Override
    public boolean pickFillsStack() {return true;}

    @Override
    public boolean pickNeverChangesSlot() {return true;}

    // drag item tweaks
    @Override
    public boolean dragMatchingStacksAcrossInventories() {return true;}

    @Override
    public boolean chooseBottomRowStacksFirst() {return true;}

    @Override
    public boolean moveHotbarStacks() {return true;}

    @Override
    public DepositType depositCursorStack() {return DepositType.ON_RELEASE_AT_EMPTY;}

    @Override
    public boolean dragSingleStackOutOfInventory() {return true;}

    @Override
    public Keyable dragMatchingStacksOutOfInventory() {return Keyable.WITH_KEY;}

    @Override
    public boolean ignoreStackComponents() {return false;}

    // offhand tweaks
    @Override
    public boolean shiftClickToOffhandStack() {return true;}

    @Override
    public boolean allFoodIsOffhandPreferred() {return true;}

    @Override
    public boolean excludeFoodWithNegativeEffects() {return true;}

    @Override
    public ImmutableList<String> offhandPreferredItems() {
        return OFFHAND_PREFERRED_ITEMS;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return NoConfigScreen::new;
    }

    public static class NoConfigScreen extends Screen {
        private static final Text NO_CONFIG_SCREEN_TITLE =
            Text.translatable("text." + NAMESPACE + ".no_config_screen.title");
        private static final Text NO_CONFIG_SCREEN_MESSAGE =
            Text.translatable("text." + NAMESPACE + ".no_config_screen.message");

        private final Screen parent;

        public NoConfigScreen(Screen parent) {
            super(NO_CONFIG_SCREEN_TITLE);

            this.parent = parent;
        }

        @Override
        public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
            super.render(graphics, mouseX, mouseY, delta);
            final int windowHCenter = MinecraftClient.getInstance().getWindow().getScaledWidth() / 2;
            final int windowHeight = MinecraftClient.getInstance().getWindow().getScaledHeight();

            //noinspection DataFlowIssue; Formatting.WHITE.getColorValue() is non-null
            final int white = Formatting.WHITE.getColorValue();
            graphics.drawCenteredShadowedText(
                this.textRenderer,
                Util.replace(NO_CONFIG_SCREEN_TITLE, "\\$\\{name\\}", InventoryControlTweaks.NAME.getString()),
                windowHCenter, windowHeight / 10,
                white
            );

            //noinspection DataFlowIssue; Formatting.RED.getColorValue() is non-null
            final int red = Formatting.RED.getColorValue();
            graphics.drawCenteredShadowedText(
                this.textRenderer,
                NO_CONFIG_SCREEN_MESSAGE,
                windowHCenter, windowHeight / 2,
                red
            );
        }

        @Override
        @SuppressWarnings("ConstantConditions")
        public void closeScreen() {
            this.client.setScreen(this.parent);
        }
    }
}
