package net.sssubtlety.inventory_control_tweaks.config;

import net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks;
import net.sssubtlety.inventory_control_tweaks.init.Networking;

import com.google.common.collect.ImmutableSet;
import com.mojang.blaze3d.platform.InputUtil;

import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.option.KeyBind;
import net.minecraft.component.DataComponentTypes;
import net.minecraft.component.type.ConsumableComponent;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.item.ApplyEffectsComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Holder;
import net.minecraft.registry.Registries;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;

import org.lwjgl.glfw.GLFW;

import com.mojang.datafixers.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.sssubtlety.inventory_control_tweaks.InventoryControlTweaks.NAMESPACE;
import static net.sssubtlety.inventory_control_tweaks.Util.isModLoaded;

import static java.lang.System.lineSeparator;

public final class ConfigManager {
    private ConfigManager() { }

    private static final String KEY_BIND_TRANSLATION_KEY_PREFIX = "key." + NAMESPACE + ".";

    private static final boolean PICK_BLOCK_PRO_LOADED = isModLoaded("pickblockpro");

    private static final Config CONFIG;

    private static ImmutableSet<Item> preventUsingItems =
        createItemSet(DefaultConfig.INSTANCE.preventUsingItems()).getFirst();

    private static ImmutableSet<Item> offhandPreferredItems =
        createItemSet(DefaultConfig.INSTANCE.offhandPreferredItems()).getFirst();

    private static ActionResult onConfigChange(ClothConfig config) {
        processConfig(config);

        return ActionResult.CONSUME;
    }

    private static ActionResult onConfigSave(ClothConfig config) {
        final ActionResult result = onConfigChange(config);

        final ClientPlayNetworkHandler networkHandler = MinecraftClient.getInstance().getNetworkHandler();
        if (networkHandler != null) {
            Networking.onSettingsSaved(
                pickFillsStack(), pickNeverChangesSlot(),
                networkHandler::send
            );
        }

        return result;
    }

    private static void processConfig(ClothConfig config) {
        final Pair<ImmutableSet<Item>, List<Identifier>> preventUsingResult =
            createItemSet(config.preventUsingItems());

        preventUsingItems = preventUsingResult.getFirst();

        final Pair<ImmutableSet<Item>, List<Identifier>> offhandPreferredResult =
            createItemSet(config.offhandPreferredItems());

        offhandPreferredItems = offhandPreferredResult.getFirst();

        final List<Identifier> missingIds = Stream
            .of(preventUsingResult.getSecond(), offhandPreferredResult.getSecond())
            .flatMap(Collection::stream)
            .toList();

        if (!missingIds.isEmpty()) {
            final String lineTab = lineSeparator() + "\t";
            final String errorMessage = missingIds.stream().map(Object::toString)
                .collect(Collectors.joining(lineTab, lineTab, ""));

            InventoryControlTweaks.LOGGER.error(
                "Error processing config; the following items don't exist: {}", errorMessage
            );
        }
    }

    /**
     * @return a set of items with the passed {@code ids} and a list of any identifiers for which no item is registered
     */
    private static Pair<ImmutableSet<Item>, List<Identifier>> createItemSet(List<String> ids) {
        final List<Identifier> missingIds = new ArrayList<>();

        final ImmutableSet<Item> itemSet = ids.stream()
            .map(Identifier::parse)
            .map(id -> {
                final Optional<Item> item = Registries.ITEM.getOrEmpty(id);
                if (item.isEmpty()) {
                    missingIds.add(id);
                }

                return item;
            })
            .flatMap(Optional::stream)
            .collect(ImmutableSet.toImmutableSet());

        return new Pair<>(itemSet, missingIds);
    }

    static {
        if (isModLoaded("cloth-config", ">=6.1.48")) {
            CONFIG = ClothConfig.register(ConfigManager::onConfigChange, ConfigManager::onConfigSave);
        } else {
            CONFIG = DefaultConfig.INSTANCE;
        }
    }

    public static final String CATEGORY_TRANSLATION_KEY = "category." + NAMESPACE;

    public static final KeyBind DRAG_MATCHING_OUT_OF_INVENTORY_MODIFIER =
        registerKeyBind("drag_out_of_inventory_modifier", GLFW.GLFW_KEY_LEFT_CONTROL);

    public static final KeyBind DRAG_ALL_OUT_OF_INVENTORY_MODIFIER =
        registerKeyBind("drag_all_out_of_inventory_modifier", GLFW.GLFW_KEY_UNKNOWN);

    public static final KeyBind CLICK_ARMOR_SWAP_MODIFIER =
        registerKeyBind("click_armor_swap_modifier", GLFW.GLFW_KEY_RIGHT_SHIFT);

    public static final KeyBind DRAG_ALL_STACKS_ACROSS_MODIFIER =
        registerKeyBind("drag_all_stacks_across_modifier", GLFW.GLFW_KEY_LEFT_ALT);

    public static void init() { }

    public static Function<Screen, ? extends Screen> getScreenFactory() {
        return CONFIG.screenFactory();
    }

    public static boolean shouldPreventUsage(Item item) {
        return preventUsingItems.contains(item);
    }

    public static boolean pickFillsStack() {
        return CONFIG.pickFillsStack();
    }

    public static boolean pickNeverChangesSlot() {
        // PBP handles slot changing if it's installed
        return !PICK_BLOCK_PRO_LOADED && CONFIG.pickNeverChangesSlot();
    }

    // drag_item_tweaks
    public static boolean dragMatchingStacksAcrossInventories() {
        return CONFIG.dragMatchingStacksAcrossInventories();
    }

    public static boolean chooseBottomRowStacksFirst() {
        return CONFIG.chooseBottomRowStacksFirst();
    }

    public static boolean moveHotbarStacks() {
        return CONFIG.moveHotbarStacks();
    }

    public static Config.DepositType depositCursorStack() {
        return CONFIG.depositCursorStack();
    }

    public static boolean dragSingleStackOutOfInventory() {
        return CONFIG.dragSingleStackOutOfInventory();
    }

    public static Config.Keyable dragMatchingStacksOutOfInventory() {
        return CONFIG.dragMatchingStacksOutOfInventory();
    }

    public static boolean ignoreStackComponents() {
        return CONFIG.ignoreStackComponents();
    }

    // offhand tweaks
    public static boolean shiftClickToOffhandStack() {
        return CONFIG.shiftClickToOffhandStack();
    }

    public static boolean isOffhandPreferred(ItemStack stack) {
        return offhandPreferredItems.contains(stack.getItem()) || isOffhandPreferredFood(stack);
    }

    private static boolean isOffhandPreferredFood(ItemStack stack) {
        if (
            !CONFIG.allFoodIsOffhandPreferred() ||
            !stack.contains(DataComponentTypes.FOOD)
        ) {
            return false;
        }

        // SUSPICIOUS_STEW uses SuspiciousStewEffectsComponent, not ConsumableComponent,
        // so we don't have to explicitly ignore its effects
        if (CONFIG.excludeFoodWithNegativeEffects()) {
            final ConsumableComponent consumable = stack.get(DataComponentTypes.CONSUMABLE);
            if (consumable != null) {
                return consumable.effects().stream()
                    .flatMap(onConsumeEffect ->
                        onConsumeEffect instanceof ApplyEffectsComponent applyEffects ?
                            Stream.of(applyEffects) :
                            Stream.empty()
                    )
                    .map(ApplyEffectsComponent::effects)
                    .flatMap(Collection::stream)
                    .map(StatusEffectInstance::getEffectType)
                    .map(Holder::getValue)
                    .allMatch(StatusEffect::isBeneficial);
            }
        }

        return true;
    }

    private static KeyBind registerKeyBind(String translationKeySuffix, int keyCode) {
        return KeyBindingHelper.registerKeyBinding(new KeyBind(
            KEY_BIND_TRANSLATION_KEY_PREFIX + translationKeySuffix,
            InputUtil.Type.KEYSYM,
            keyCode,
            CATEGORY_TRANSLATION_KEY
        ));
    }
}
