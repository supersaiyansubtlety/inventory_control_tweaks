package net.sssubtlety.inventory_control_tweaks.mixin.accessor;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.screen.ScreenHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(CraftingInventory.class)
public interface CraftingInventoryAccessor {
    @Accessor("handler")
    ScreenHandler inventory_control_tweaks$getHandler();
}
