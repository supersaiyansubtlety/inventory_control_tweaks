package net.sssubtlety.inventory_control_tweaks.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen.CreativeScreenHandler;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;

import net.sssubtlety.inventory_control_tweaks.ClientUtil;
import net.sssubtlety.inventory_control_tweaks.config.Config;
import net.sssubtlety.inventory_control_tweaks.config.ConfigManager;
import net.sssubtlety.inventory_control_tweaks.Util;
import net.sssubtlety.inventory_control_tweaks.mixin_helper.MouseDraggedMixinAccessor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.function.Function;

import static net.minecraft.item.ItemStack.itemsAndComponentsMatch;
import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.DRAG_MATCHING_OUT_OF_INVENTORY_MODIFIER;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.clickAllSlots;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.clickEachSlot;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.isKeyPressed;

import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.PUTFIELD;

@Mixin(HandledScreen.class)
abstract class HandledScreenMouseReleasedMixin <T extends ScreenHandler> extends Screen implements
        ScreenHandlerProvider<T> {
    @Unique private static final int NO_SLOT_INDICATOR = -999;

    @Shadow @Final protected T handler;
    @Shadow private boolean doubleClicking;

    private HandledScreenMouseReleasedMixin() {
        super(null);
        throw new IllegalStateException("HandledScreenMouseReleasedMixin's dummy constructor called!");
    }

    @ModifyExpressionValue(
        method = "mouseReleased",
        // method_64240 is getHoveredSlot, whose result is assigned to a Slot local at the start of mouseReleased
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;" +
                "method_64240(DD)Lnet/minecraft/screen/slot/Slot;"
        )
    )
    private Slot captureHoveredSlot(Slot original, @Share("hoveredSlot") LocalRef<Slot> hoveredSlot) {
        hoveredSlot.set(original);
        return original;
    }

    @ModifyExpressionValue(
        method = "mouseReleased",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;isClickOutsideBounds(DDIII)Z"
        )
    )
    private boolean captureOutsideBounds(boolean original, @Share("outsideBounds") LocalBooleanRef outsideBounds) {
        outsideBounds.set(original);
        return original;
    }

    @Inject(
        method = "mouseReleased", require = 1, allow = 1,
        at = @At(
            value = "FIELD", opcode = PUTFIELD,
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;cancelNextRelease:Z"
        ),
        slice = @Slice(
            from = @At(
                value = "FIELD", opcode = GETFIELD, shift = At.Shift.AFTER,
                target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;cancelNextRelease:Z"
            ),
            to = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/client/option/GameOptions;getTouchscreen()Lnet/minecraft/client/option/Option;"
            )
        )
    )
    private void onMouseReleaseCancel(
        double mouseX, double mouseY, int button, CallbackInfoReturnable<Boolean> cir,
        @Share("hoveredSlot") LocalRef<Slot> hoveredSlot, @Share("outsideBounds") LocalBooleanRef outsideBounds
    ) {
        if (
            this.client == null ||
            this.client.player == null ||
            button != 0 ||
            this.doubleClicking
        ) {
            return;
        }

        final ItemStack cursorStack = this.client.player.currentScreenHandler.getCursorStack();

        if (outsideBounds.get()) {
            this.onReleaseOutsideBounds(cursorStack);
        } else {
            this.onReleaseInsideBounds(hoveredSlot.get(), cursorStack);
        }

        // reset unconditionally at end
        ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$endMove();
    }

    @Unique
    private void onReleaseOutsideBounds(ItemStack cursorStack) {
        if (cursorStack.isEmpty()) {
            return;
        }

        final Config.Keyable dragMatchingOut = ConfigManager.dragMatchingStacksOutOfInventory();

        assert (this.client != null);
        assert (this.client.player != null);
        assert (this.client.interactionManager != null);

        final boolean holdingThrowMatching = dragMatchingOut == Config.Keyable.WITH_KEY &&
            isKeyPressed(DRAG_MATCHING_OUT_OF_INVENTORY_MODIFIER, this.client);

        final boolean shouldThrowMatching = dragMatchingOut == Config.Keyable.NO_KEY || holdingThrowMatching;

        final boolean holdingThrowAll = isKeyPressed(ConfigManager.DRAG_ALL_OUT_OF_INVENTORY_MODIFIER, this.client);

        final boolean noThrowKeyHeld = !(holdingThrowMatching || holdingThrowAll);
        if (((MouseDraggedMixinAccessor) this).inventory_control_tweaks$hasMovedStacks() && noThrowKeyHeld) {
            // if stacks moved and no throw key held, prevent throwing and instead deposit cursor stack
            // this is to prevent accidental throwing (a key being held is taken to indicate it isn't an accident)

            ClientUtil.depositRemainder(
                cursorStack,
                ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$getDestination(),
                ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$getSource(),
                this.client.player.getInventory(),
                this.handler
            );

            return;
        }

        if (!(shouldThrowMatching || ConfigManager.dragSingleStackOutOfInventory() || holdingThrowAll)) {
            return;
        }

        // throw cursor stack
        final boolean isCreativeInv;
        if (this.client.player.currentScreenHandler instanceof CreativeScreenHandler creativeScreenHandler) {
            isCreativeInv = true;

            this.client.player.dropItem(cursorStack, true);
            this.client.interactionManager.dropCreativeStack(cursorStack);
            creativeScreenHandler.setCursorStack(ItemStack.EMPTY);
        } else {
            isCreativeInv = false;

            ClientUtil.clickSlotId(NO_SLOT_INDICATOR, SlotActionType.PICKUP, this.handler);
        }

        if (shouldThrowMatching) {
            @Nullable
            final Inventory destination = ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$getDestination();
            final Function<Slot, Integer> indexer = isCreativeInv ? Slot::getIndex : Util::getId;
            clickEachSlot(
                this.handler.slots, this.client.player, destination, this.client.interactionManager,
                1, SlotActionType.THROW, this.handler,
                indexer, cursorStack
            );
        } else if (holdingThrowAll) {
            clickAllSlots(
                this.handler.slots, this.client.player,
                ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$getDestination(),
                this.client.interactionManager, 1, SlotActionType.THROW, this.handler,
                isCreativeInv ? Slot::getIndex : Util::getId
            );
        }
    }

    @Unique
    private void onReleaseInsideBounds(@Nullable Slot hoveredSlot, ItemStack cursorStack) {
        final Config.DepositType depositType = ConfigManager.depositCursorStack();
        if (
            depositType == Config.DepositType.DISABLED ||
            depositType == Config.DepositType.IMMEDIATELY_AT_EMPTY ||
            !((MouseDraggedMixinAccessor) this).inventory_control_tweaks$hasCursorCrossedInventories()
        ) {
            return;
        }

        final Inventory destination = ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$getDestination();

        if (hoveredSlot != null) {
            if (destination != hoveredSlot.inventory) {
                return;
            }

            if (depositType == Config.DepositType.AT_RELEASE_SLOT) {
                final ItemStack hoveredStack = hoveredSlot.getStack();
                if (hoveredStack.isEmpty() || itemsAndComponentsMatch(hoveredStack, cursorStack)) {
                    ClientUtil.clickSlotId(hoveredSlot.id, SlotActionType.PICKUP, this.handler);
                }
            }
        }

        if (!cursorStack.isEmpty()) {
            // AT_EMPTY_SLOT or stack didn't fit AT_HOVERED_SLOT or hoveredSlot == null
            assert (this.client != null);
            assert (this.client.player != null);

            ClientUtil.depositRemainder(
                cursorStack, destination,
                ((MouseDraggedMixinAccessor) this).inventory_control_tweaks$getSource(),
                this.client.player.getInventory(), this.handler
            );
        }
    }
}
