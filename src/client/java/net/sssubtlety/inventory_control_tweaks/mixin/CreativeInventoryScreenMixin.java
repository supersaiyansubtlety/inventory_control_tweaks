package net.sssubtlety.inventory_control_tweaks.mixin;

import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.sssubtlety.inventory_control_tweaks.mixin_helper.HandledScreenMouseClickedMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(CreativeInventoryScreen.class)
class CreativeInventoryScreenMixin {
    // same mixin is in HandledScreenMouseClickedMixin because
    // CreativeInventoryScreen @Overrides the onMouseClick of HandledScreen
    @Inject(method = "onMouseClick", at = @At("HEAD"), cancellable = true)
    private void cancelMouseClickSlotIfReplaced(
        Slot slot, int slotId, int button, SlotActionType actionType, CallbackInfo ci
    ) {
        if (((HandledScreenMouseClickedMixinAccessor)this).inventory_control_tweaks$didReplaceMouseClickSlot()) {
            ci.cancel();
        }
    }
}
