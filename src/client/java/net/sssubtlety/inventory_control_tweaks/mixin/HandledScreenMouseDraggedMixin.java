package net.sssubtlety.inventory_control_tweaks.mixin;

import com.google.common.collect.Lists;
import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;

import net.sssubtlety.inventory_control_tweaks.ClientUtil;
import net.sssubtlety.inventory_control_tweaks.config.Config;
import net.sssubtlety.inventory_control_tweaks.config.ConfigManager;
import net.sssubtlety.inventory_control_tweaks.Util;
import net.sssubtlety.inventory_control_tweaks.mixin.accessor.CraftingInventoryAccessor;
import net.sssubtlety.inventory_control_tweaks.mixin_helper.MouseDraggedMixinAccessor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;

import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.DRAG_ALL_STACKS_ACROSS_MODIFIER;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.clickEachSlot;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.isKeyPressed;

@Mixin(HandledScreen.class)
abstract class HandledScreenMouseDraggedMixin<T extends ScreenHandler> extends Screen implements
        ScreenHandlerProvider<T>, MouseDraggedMixinAccessor {
    @Unique private boolean movedStacks;
    @Unique private boolean cursorCrossedInventories;
    @Unique @Nullable private Inventory source;
    @Unique @Nullable private Inventory destination;

    @Shadow protected boolean cursorDragging;
    @Shadow @Final protected T handler;
    @Shadow private boolean doubleClicking;

    private HandledScreenMouseDraggedMixin() {
        super(null);
        throw new IllegalStateException("HandledScreenMouseDraggedMixin's dummy constructor called!");
    }

    @Override
    public @Nullable Inventory inventory_control_tweaks$getSource() {
        return this.source;
    }

    @Override
    public @Nullable Inventory inventory_control_tweaks$getDestination() {
        return this.destination;
    }

    @Override
    public boolean inventory_control_tweaks$hasMovedStacks() {
        return this.movedStacks;
    }

    @Override
    public boolean inventory_control_tweaks$hasCursorCrossedInventories() {
        return this.cursorCrossedInventories;
    }

    @Override
    public void inventory_control_tweaks$endMove() {
        this.destination = null;
        this.movedStacks = false;
        this.cursorCrossedInventories = false;
    }

    @ModifyExpressionValue(method = "mouseDragged", at = @At(
        value = "INVOKE",
        target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;" +
            "method_64240(DD)Lnet/minecraft/screen/slot/Slot;"
    ))
    private Slot mouseDraggedTryMoveAll(
        Slot hoveredSlot, double mouseX, double mouseY, int button, double deltaX, double deltaY
    ) {
        this.mouseDraggedTryMoveAllImpl(hoveredSlot, button);
        return hoveredSlot;
    }

    @Unique
    private void mouseDraggedTryMoveAllImpl(Slot hoveredSlot, int button) {
        if (this.client == null || this.doubleClicking || button != 0) {
            return;
        }

        final boolean transferAll = isKeyPressed(DRAG_ALL_STACKS_ACROSS_MODIFIER, this.client);
        if (
            (
                transferAll ||
                ConfigManager.dragMatchingStacksAcrossInventories()
            )  &&
            (this.client.options != null) &&
            !this.client.options.getTouchscreen().get() &&
            !this.cursorDragging &&
            hoveredSlot != null
        ) {
            final ItemStack cursorStack = this.handler.getCursorStack();
            if (!cursorStack.isEmpty()) {
                if (this.destination == null) {
                    this.source = hoveredSlot.inventory;
                    this.destination = this.source;
                } else if (
                    !(
                        hoveredSlot.inventory instanceof CraftingInventory &&
                        ((CraftingInventoryAccessor) hoveredSlot.inventory)
                            .inventory_control_tweaks$getHandler() instanceof PlayerScreenHandler
                    ) &&
                        this.destination != hoveredSlot.inventory
                ) {
                    if (this.client.player == null) {
                        return;
                    }

                    if (this.client.interactionManager == null) {
                        return;
                    }

                    final ItemStack originalCursorStack;
                    final boolean despotImmediately =
                        ConfigManager.depositCursorStack() == Config.DepositType.IMMEDIATELY_AT_EMPTY;
                    if (despotImmediately) {
                        originalCursorStack = cursorStack.copy();
                        ClientUtil.depositRemainder(
                            cursorStack, hoveredSlot.inventory, this.source,
                            this.client.player.getInventory(), this.handler
                        );
                    } else {
                        originalCursorStack = cursorStack;
                    }

                    final boolean movedStacks = clickEachSlot(
                        ConfigManager.chooseBottomRowStacksFirst() ?
                            Lists.reverse(this.handler.slots) :
                            this.handler.slots,
                        this.client.player,
                        this.destination,
                        this.client.interactionManager,
                        button,
                        SlotActionType.QUICK_MOVE,
                        this.handler,
                        Util::getId,
                        transferAll ? null : originalCursorStack
                    );

                    this.source = this.destination;
                    if (despotImmediately) {
                        this.inventory_control_tweaks$endMove();
                    } else {
                        this.destination = hoveredSlot.inventory;
                        this.movedStacks = movedStacks;
                        this.cursorCrossedInventories = true;
                    }
                }
            }
        }
    }
}
