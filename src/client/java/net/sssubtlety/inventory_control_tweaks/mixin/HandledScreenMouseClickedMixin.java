package net.sssubtlety.inventory_control_tweaks.mixin;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen.CreativeScreenHandler;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;

import net.sssubtlety.inventory_control_tweaks.config.ConfigManager;
import net.sssubtlety.inventory_control_tweaks.mixin.accessor.CreativeSlotAccessor;
import net.sssubtlety.inventory_control_tweaks.mixin_helper.CreativeSlotCheckable;
import net.sssubtlety.inventory_control_tweaks.mixin_helper.HandledScreenMouseClickedMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.CLICK_ARMOR_SWAP_MODIFIER;
import static net.sssubtlety.inventory_control_tweaks.Util.getPlayerSlotId;
import static net.sssubtlety.inventory_control_tweaks.Util.getPlayerSlotIndex;
import static net.sssubtlety.inventory_control_tweaks.Util.isFull;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.isKeyPressed;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.trySwapEquipment;

@Mixin(HandledScreen.class)
abstract class HandledScreenMouseClickedMixin<T extends ScreenHandler> extends Screen implements
        ScreenHandlerProvider<T>, HandledScreenMouseClickedMixinAccessor {
    @Unique
    private boolean replacedMouseClickSlot;

    @Shadow
    protected abstract void onMouseClick(Slot slot, int invSlot, int clickData, SlotActionType actionType);

    @Shadow
    protected Slot focusedSlot;

    private HandledScreenMouseClickedMixin() {
        super(null);
        throw new IllegalStateException("HandledScreenMixin's dummy constructor called! ");
    }

    @ModifyArg(
        method = "mouseClicked",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;onMouseClick(" +
                "Lnet/minecraft/screen/slot/Slot;IILnet/minecraft/screen/slot/SlotActionType;)V"
        ),
        slice = @Slice(
            from = @At(
                value = "FIELD",
                target = "Lnet/minecraft/screen/slot/SlotActionType;THROW:Lnet/minecraft/screen/slot/SlotActionType;"
            ),
            to = @At(value = "FIELD", target = "Lnet/minecraft/client/gui/screen/ingame/HandledScreen;heldButtonCode:I")
        )
    )
    private Slot preMouseClickSlot(Slot slot, int invSlot, int clickData, SlotActionType slotActionType) {
        assert (this.client != null);
        assert (this.client.player != null);

        final boolean tryReplace =
            slot != null &&
            // left click
            clickData == 0 && (
                // own inventory or creative inventory without creative tab open
                this.client.player.playerScreenHandler == this.client.player.currentScreenHandler || (
                    ((this.client.player.currentScreenHandler instanceof CreativeScreenHandler creativeHandler) &&
                    creativeHandler.itemList.isEmpty())
                )
            );

        this.replacedMouseClickSlot =
            tryReplace && (
                (
                    // try shift-click to offhand
                    slotActionType == SlotActionType.QUICK_MOVE &&
                    this.tryMovingToOffhand(slot)
                ) || (
                    // try armor swap
                    isKeyPressed(CLICK_ARMOR_SWAP_MODIFIER, this.client) &&
                    trySwapEquipment(
                        this.client.player, slot.getStack(),
                        getPlayerSlotIndex(
                            slot instanceof CreativeSlotCheckable ?
                                ((CreativeSlotAccessor) slot).inventory_control_tweaks$getSlot().id :
                                slot.id
                        )
                    )
                )
            );
        // no change, just using ModifyArg to capture method args
        return slot;
    }

    // same mixin is in CreativeInventoryScreenMixin because CreativeInventoryScreen @Overrides onMouseClick
    @Inject(
        method = "onMouseClick(Lnet/minecraft/screen/slot/Slot;IILnet/minecraft/screen/slot/SlotActionType;)V",
        cancellable = true, at = @At(value = "HEAD")
    )
    private void cancelMouseClickSlotIfReplaced(
        Slot slot, int slotId, int button, SlotActionType actionType, CallbackInfo ci
    ) {
        if (this.inventory_control_tweaks$didReplaceMouseClickSlot()) {
            ci.cancel();
        }
    }

    // returns true if alternate behavior was successful (meaning vanilla behavior should not also happen)
    @Unique
    private boolean tryMovingToOffhand(Slot slot) {
        if (
            this.client == null ||
            this.client.interactionManager == null ||
            this.client.player == null
        ) {
            return false;
        }

        if (slot.inventory != this.client.player.getInventory()) {
            return false;
        }

        if (this.client.player.isCreative()) {
            if (slot.getIndex() == 45) {
                return false;
            }
        } else if (slot.getIndex() == PlayerInventory.OFF_HAND_SLOT) {
            return false;
        }

        final ItemStack offHandStack = this.client.player.getOffHandStack();
        final ItemStack clickedStack = slot.getStack();

        if (offHandStack.isEmpty()) {
            if (ConfigManager.isOffhandPreferred(clickedStack)) {
                // swap with (empty) offhand
                this.onMouseClick(
                    this.focusedSlot, this.focusedSlot.id,
                    PlayerInventory.OFF_HAND_SLOT, SlotActionType.SWAP
                );
                this.getScreenHandler().sendContentUpdates();

                return true;
            }
        } else if (
            ConfigManager.shiftClickToOffhandStack() && (
                // room in stack in offHand
                !isFull(offHandStack) &&
                // can combine with offhand
                ItemStack.itemsAndComponentsMatch(offHandStack, clickedStack)
            )
        ) {
            final int index = this.focusedSlot.getIndex();
            final int adjustedId = getPlayerSlotId(index);
            this.onMouseClick(null, adjustedId, 0, SlotActionType.PICKUP);
            this.onMouseClick(null, 45, 0, SlotActionType.PICKUP);
            this.onMouseClick(null, adjustedId, 0, SlotActionType.PICKUP);

            return true;
        }

        return false;
    }

    @Override
    public boolean inventory_control_tweaks$didReplaceMouseClickSlot() {
        if (this.replacedMouseClickSlot) {
            this.replacedMouseClickSlot = false;
            return true;
        } else {
            return false;
        }
    }
}
