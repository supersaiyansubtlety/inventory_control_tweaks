package net.sssubtlety.inventory_control_tweaks.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.v2.WrapWithCondition;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.network.PlayerActionPacketFactory;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;

import org.apache.commons.lang3.mutable.MutableObject;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import static net.sssubtlety.inventory_control_tweaks.config.ConfigManager.shouldPreventUsage;

@Environment(EnvType.CLIENT)
@Mixin(ClientPlayerInteractionManager.class)
abstract class ClientPlayerInteractionManagerMixin {
    // init mutable object so interactItem doesn't return null when we cancel sendActionPacket
    @ModifyExpressionValue(
        method = "interactItem",
        at = @At(
            value = "NEW", remap = false,
            target = "()Lorg/apache/commons/lang3/mutable/MutableObject;"
        )
    )
    private MutableObject<Object> initMutableObject(MutableObject<Object> original) {
        original.setValue(ActionResult.PASS);
        return original;
    }

    @WrapWithCondition(
        method = "interactItem",
        at = @At(value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;sendActionPacket(" +
                "Lnet/minecraft/client/world/ClientWorld;Lnet/minecraft/client/network/PlayerActionPacketFactory;)V"
        )
    )
    private boolean limitItemUsage(
        ClientPlayerInteractionManager instance, ClientWorld world,
        PlayerActionPacketFactory packetFactory, PlayerEntity player, Hand hand
    ) {
        return !shouldPreventUsage(player.getStackInHand(hand).getItem());
    }
}
