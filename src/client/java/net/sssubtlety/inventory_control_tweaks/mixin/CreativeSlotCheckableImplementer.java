package net.sssubtlety.inventory_control_tweaks.mixin;

import net.sssubtlety.inventory_control_tweaks.mixin_helper.CreativeSlotCheckable;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(targets = "net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen$CreativeSlot")
abstract class CreativeSlotCheckableImplementer implements CreativeSlotCheckable { }
