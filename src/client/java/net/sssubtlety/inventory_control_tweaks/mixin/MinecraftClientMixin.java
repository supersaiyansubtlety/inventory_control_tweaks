package net.sssubtlety.inventory_control_tweaks.mixin;

import net.sssubtlety.inventory_control_tweaks.config.ConfigManager;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.math.BlockPos;

import com.llamalad7.mixinextras.injector.v2.WrapWithCondition;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import static net.sssubtlety.inventory_control_tweaks.ClientUtil.tryReplaceClientBlockPick;
import static net.sssubtlety.inventory_control_tweaks.ClientUtil.tryReplaceClientEntityPick;

/**
 * The lowest priority {@link WrapWithCondition} injections are outermost.<br>
 * This has low priority so these {@link WrapWithCondition}s wrap Pick Block Pro's injections.
 * <p>
 * This must be called before PBP's injects to ensure {@link PlayerInventory#selectedSlot} hasn't been modified yet.<br>
 * If PBP is installed, it handles {@link PlayerInventory#selectedSlot}, but this mixin needs to know the original
 * {@link PlayerInventory#selectedSlot} before any modifications, so if the picked stack is already held, it can be
 * filled if {@link ConfigManager#pickFillsStack} is {@code true}.
 */
@Mixin(value = MinecraftClient.class, priority = 500)
abstract class MinecraftClientMixin {
    @WrapWithCondition(
        method = "doItemPick",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;" +
                "method_65193(Lnet/minecraft/util/math/BlockPos;Z)V"
        )
    )
    private boolean modifyBlockPick(
        ClientPlayerInteractionManager instance, BlockPos pos, boolean includeData
    ) {
        return tryReplaceClientBlockPick((MinecraftClient) (Object) this, instance, pos, includeData);
    }

    @WrapWithCondition(
        method = "doItemPick",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayerInteractionManager;" +
                "method_2916(Lnet/minecraft/entity/Entity;Z)V"
        )
    )
    private boolean modifyEntityPick(
        ClientPlayerInteractionManager instance, Entity entity, boolean includeData
    ) {
        return tryReplaceClientEntityPick((MinecraftClient) (Object) this, instance, entity, includeData);
    }
}
